from typing import Iterable
from typing import Sequence
from collections import defaultdict

import matplotlib.pyplot as plt

ValueType = int | float
ProbaType = float


def make_pmf_from_iterable(data: Iterable) -> dict[ValueType, ProbaType]:
    hist = defaultdict(int)
    for item in data:
        hist[item] += 1
    sample_sum = sum(hist.values())
    for item in hist:
        hist[item] /= sample_sum
    return dict(hist)


class Pmf:
    def __init__(
            self,
            data: dict[int, float],
            label: str = 'Unknown distribution',
    ):
        self._label = label
        self._data = data

    def __str__(self) -> str:
        return f'PMF of {self._label}'

    @property
    def values(self) -> Sequence[int]:
        return list(self._data.keys())

    @property
    def probs(self) -> Sequence[float]:
        return list(self._data.values())

    def draw_bar(self, xlim=None, xlabel='Unknown', color=None):
        # TODO: support step function
        plt.bar(self.values, self.probs, label=self._label, color=color)
        plt.xlabel(xlabel)
        plt.ylabel('Frequency')
        plt.xlim(xlim)
        plt.legend()


class Cdf:
    def __init__(
            self,
            data: dict[int, float],
            label: str = 'Unknown distribution',
    ):
        pass


def draw_several_pmf(*args: Sequence[Pmf]):
    pass
