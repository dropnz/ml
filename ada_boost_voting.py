import numpy as np
from sklearn.base import BaseEstimator


class AdaBoostVoting(BaseEstimator):
    def __init__(self, predictors):
        self.predictors = predictors

    def fit(self, X, y):
        available_predictors = self.predictors.copy()
        self._weights = np.array([1/len(y) for _ in range(len(y))])
        print(f"inited weights: {self._weights}")

    def predict(self, X):
        pass
