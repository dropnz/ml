from enum import Enum

from sklearn import tree


PType = Enum('PType', ['CONT', 'INT', 'DISCRETE'])

ALPHA = {"alpha": {"value": 0.5, "min_eq": 1.0e-10, "max_eq": 1.0, "add": [0.05, 0.95], "type": PType.CONT}}  # default: 1.0, 0 for no smoothing
BINARIZE = {"binarize": {"value": 0.0, "min_eq": 0.0, "max_eq": 1.0, "add": [0.5, 1.0], "type": PType.CONT}}  # float or None, optional (default=0.0)
BOOTSTRAP = {"bootstrap": [True, False]}
EARLY_STOPPING = {"early_stopping": [True, False]}  # Whether to use early stopping to terminate training when validation score is not improving
FIT_INTERCEPT = {"fit_intercept": [True, False]}  # bool, default=False
FIT_PRIOR = {"fit_prior": [True, False]}
MAX_DEPTH = {"max_depth": {"value": 5, "add": 20, "type": PType.INT, "min": 1}}
MAX_ITER = {"max_iter": {"value": 100, "type": PType.INT, "add": 500, "min_eq": 1}}
MIN_SAMPLES_LEAF = {"min_samples_leaf": {"value": 0.01, "min_eq": 0, "max_eq": 1., "type": PType.CONT, "add": 0.1}}
MIN_SAMPLES_SPLIT = {"min_samples_split": {"value": 0.5, "min_eq": 0, "max_eq": 1., "type": PType.CONT}}
N_ESTIMATORS = {"n_estimators": {"value": 50, "add": 500, "type": PType.INT, "min": 0}}
N_ITER_NO_CHANGE = {"n_iter_no_change": {"value": 5, "min_eq": 1, "type": PType.INT, "add": 10}}
POWER_T = {"power_t": {"value": 0.5,  "type": PType.CONT}}  # The exponent for inverse scaling learning rate. It is used in updating effective learning rate when the learning_rate is set to ‘invscaling’. Only used when solver=’sgd’.
SHUFFLE = {"shuffle": [True, False]}  # Whether to shuffle samples in each iteration
TOL = {"tol": {"value": 1e-4, "type": PType.CONT, "min": 0, "add": [1e-7, 1e-7]}}
VALIDATION_FRACTION = {"validation_fraction": {"value": 0.1, "type": PType.CONT, "add": 0.5, "min_eq": 0, "max_eq": 1}}  # The proportion of training data to set aside as validation set for early stopping. Must be between 0 and 1. Only used if early_stopping is True
WARM_START = {"warm_start": [True, False]}  # bool, optional
C_1 = {"C": {"value": 1.0,  "type": PType.CONT, "add": [0.1, 100, 500], "min": 0.0}}
DUAL = {"dual": [True, False]}  # default=False. Dual or primal formulation. Dual formulation is only implemented for l2 penalty with liblinear solver. Prefer dual=False when n_samples > n_features.
LIBLINEAR_INTERCEPT_SCALING = {"intercept_scaling": {"value": 1.0, "type": PType.CONT, "min": 0.0}}  # Useful only when the solver ‘liblinear’ is used and self.fit_intercept is set to True. In this case, x becomes [x, self.intercept_scaling], i.e. a “synthetic” feature with constant value equal to intercept_scaling is appended to the instance vector. The intercept becomes intercept_scaling * synthetic_feature_weight.
ELASTICNET_L1_RATIO = {"l1_ratio": {"value": 0.5, "add": [0.1, 0.9], "type": PType.CONT, "min_eq": 0, "max_eq": 1}}   # The Elastic-Net mixing parameter, with 0 <= l1_ratio <= 1. Only used if penalty='elasticnet'. Setting l1_ratio=0 is equivalent to using penalty='l2', while setting l1_ratio=1 is equivalent to using penalty='l1'. For 0 < l1_ratio <1, the penalty is a combination of L1 and L2.
MIN_WEIGHT_FRACTION_LEAF = {"min_weight_fraction_leaf": {"value": 0.0, "add": 0.1, "type": PType.CONT, "min_eq": 0., "max_eq": 0.5}}  # default=0, The minimum weighted fraction of the sum total of weights (of all the input samples) required to be at a leaf node. Samples have equal weight when sample_weight is not provided.
TREE_MAX_FEATURES = {"max_features": ["sqrt", "log2", None, 0.1, 0.5, 0.9]}  # int, float, string or None, The number of features to consider when looking for the best split. The search for a split does not stop until at least one valid partition of the node samples is found, even if it requires to effectively inspect more than max_features features.
MAX_LEAF_NODES = {"max_leaf_nodes": {"value": 20, "add": [5, 25, 50], "min": 1, "type": PType.INT}}  # int or None, optional (default=None). Grow a tree with max_leaf_nodes in best-first fashion. Best nodes are defined as relative reduction in impurity. If None then unlimited number of leaf nodes.
MIN_IMPURITY_DECREASE = {"min_impurity_decrease": {"value": 0.0, "min_eq": 0.0, "add": 0.5, "type": PType.CONT}} # float, optional, default=0.0, A node will be split if this split induces a decrease of the impurity greater than or equal to this value.
CCP_ALPHA = {"ccp_alpha": {"value": 0.0, "min_eq": 0.0, "type": PType.CONT, "add": 0.5}}  # non-negative float, default=0.0, Complexity parameter used for Minimal Cost-Complexity Pruning. The subtree with the largest cost complexity that is smaller than ccp_alpha will be chosen.
VAR_SMOOTHING = {"var_smoothing": {"value": 1e-9, "type": PType.CONT, "min": 0, "add": 0.5}}


estimators_description = {
    "BernoulliNB": ALPHA | BINARIZE | FIT_PRIOR,
    "GaussianNB":  VAR_SMOOTHING,
    "MultinomialNB": ALPHA | FIT_PRIOR,
    "ComplementNB": ALPHA | FIT_PRIOR | {"norm": [True, False]},
    "KNeighborsClassifier": {
        "n_neighbors": {"value": 5, "add": [2, 10], "type": PType.INT, "min": 1},
        "weights": ["uniform", "distance"],   # str or callable, optional (default = "uniform")
        "algorithm": ["ball_tree", "kd_tree", "brute"],
        "leaf_size": {"value": 30, "type": PType.INT, "min": 0},  # int, optional (default = 30)
        "metric": ["euclidean", "manhattan", "chebyshev"],
        # TODO: metrics
        # for real-valued vector spaces: “euclidean”, “manhattan”, “chebyshev”, “minkowski”, “wminkowski”, “seuclidean”, “mahalanobis”
        # for two-dimensional vector spaces: haversine
        # for integer-valued vector spaces: “hamming”, “canberra”, “braycurtis”
        # for boolean-valued vector spaces: “jaccard”, “matching”, “dice”, “kulsinski”, “rogerstanimoto”, “russellrao”, “sokalmichener”, “sokalsneath”
        # pyfunc: User-defined distance
    },
    "RadiusNeighborsClassifier": {
        "radius": {"value": 1.0, "type": PType.CONT, "min": 0, "add": 5},
        "weights": ["uniform", "distance"],   # str or callable, optional (default = "uniform")
        "algorithm": ["ball_tree", "kd_tree", "brute"],
        "leaf_size": {"value": 30, "type": PType.INT, "min": 0},  # int, optional (default = 30)
        "p": [1, 2],
        # TODO: add metric: str or callable, default=’minkowski’
        # TODO: add metric_params dict
        "outlier_label": [None],  # label for outlier samples (samples with no neighbors in given radius).
    },
    "RandomForestClassifier":
        # let's use default parameter. i think they give same result. # "criterion": ["entropy", "gini"],  # default: gini
        # Out of bag estimation only available if bootstrap=True
        [
            N_ESTIMATORS
            | MAX_DEPTH
            | MIN_SAMPLES_LEAF
            | MIN_SAMPLES_SPLIT
            | MIN_WEIGHT_FRACTION_LEAF
            | TREE_MAX_FEATURES
            | MAX_LEAF_NODES
            | MIN_IMPURITY_DECREASE
            | {"bootstrap": [False]}
            | {"oob_score": [False]}
            | WARM_START,

            N_ESTIMATORS
            | MAX_DEPTH
            | MIN_SAMPLES_LEAF
            | MIN_SAMPLES_SPLIT
            | MIN_WEIGHT_FRACTION_LEAF
            | TREE_MAX_FEATURES
            | MAX_LEAF_NODES
            | MIN_IMPURITY_DECREASE
            | {"bootstrap": [True]}
            | {"oob_score": [True, False]}
            | WARM_START,
        ],
    "AdaBoostClassifier":
        N_ESTIMATORS
        | {"learning_rate": {"value": 1.0, "type": PType.CONT, "min": .0}}  # default: 1.0
        # If "SAMME.R" then use the SAMME.R real boosting algorithm.
        # base_estimator must support calculation of class probabilities.
        # If "SAMME" then use the SAMME discrete boosting algorithm.
        # The SAMME.R algorithm typically converges faster than SAMME,
        # achieving a lower test error with fewer boosting iterations.
        | {
            "base_estimator": [
                tree.DecisionTreeClassifier(max_depth=1),
                tree.DecisionTreeClassifier(max_depth=2),
                tree.DecisionTreeClassifier(max_depth=3),
            ],
        }
        | {"algorithm": ["SAMME", "SAMME.R"]},
    "RidgeClassifier":
        # Classifier using Ridge regression.
        # This classifier first converts the target values into ``{-1, 1}`` and
        # then treats the problem as a regression task (multi-output regression in the multiclass case).
        {"alpha": {"value": 1.0, "type": PType.CONT, "min_eq": .0, "add": [0.2, 10]}}
        | FIT_INTERCEPT | {"normalize": [True, False]} | MAX_ITER | TOL
        # TODO: add class_weight : dict or 'balanced', default=None
        | {"solver": ['svd', 'cholesky', 'lsqr', 'sparse_cg', 'sag', 'saga']},
    "GaussianProcessClassifier":
        # "kernel": None,  # TODO: add lkernel object
        # "optimizer": ["fmin_l_bfgs_b"],  #  or callable, default='fmin_l_bfgs_b'
        {"n_restarts_optimizer": {"value": 0, "min_eq": 0, "add": 5, "type": PType.INT}}
        | MAX_ITER | WARM_START,
    "GradientBoostingClassifier":
        # TODO: add init estimator or "zero", optional (default=None)
        # min_impurity_split deprecated
        {"learning_rate": {"value": 0.1, "add": 0.5, "type": PType.CONT, "min": .0}} # float, optional (default=0.1)
        | {"loss": ["deviance", "exponential"]}  # default="deviance"
        | MAX_DEPTH
        | TREE_MAX_FEATURES
        | MAX_LEAF_NODES
        | MIN_IMPURITY_DECREASE
        | MIN_SAMPLES_LEAF
        | MIN_SAMPLES_SPLIT
        | MIN_WEIGHT_FRACTION_LEAF
        | N_ESTIMATORS
        | N_ITER_NO_CHANGE
        | {"subsample": {"value": 1.0, "type": PType.CONT, "min": 0, "max_eq": 1, "add": 0.5}}  # float, optional (default=1.0)
        | TOL
        | VALIDATION_FRACTION
        | WARM_START
        | {"criterion": ["friedman_mse", "mse"]},
    "SGDClassifier":
        # параметр loss задаёт тип линейного классификатора
        {"loss": [
            # parameters allow using `predict_proba`
            "log",
            "modified_huber",
            # doesn't allow to use `predict_proba` method
            "hinge",
            "squared_hinge", "perceptron",
            "squared_loss",
            "huber",
            "epsilon_insensitive",
            "squared_epsilon_insensitive",
        ]}
        | {"penalty": ["l2", "l1", "elasticnet"]}  # default=l2
        | {"alpha": {"value": 0.001, "type": PType.CONT, "add": [1.0, 10.0], "min": 0}}
        | TOL
        | ELASTICNET_L1_RATIO
        | FIT_INTERCEPT
        | MAX_ITER
        | SHUFFLE
        | {"eta0": {"value": 0.1, "min": 0.0, "type": PType.CONT}}
        # TODO: tol, default=1e-3
        | {"learning_rate": ["constant", "optimal", "invscaling", "adaptive"]}  # default="optimal"
        | {"epsilon": {"value": 0.1, "type": PType.CONT}}
        | POWER_T
        | EARLY_STOPPING
        | VALIDATION_FRACTION
        | N_ITER_NO_CHANGE
        # TODO: class_weight, {class_label: weight} or “balanced”, default=None
        | WARM_START
        | {"average": [True, False, 3, 5, 10]},
    "ExtraTreesClassifier":
        N_ESTIMATORS
        # let's use default parameter. i think they give same result # "criterion": ["entropy", "gini"],  # default: gini
        | MAX_DEPTH
        | MIN_SAMPLES_SPLIT
        | MIN_SAMPLES_LEAF
        | MIN_WEIGHT_FRACTION_LEAF
        | TREE_MAX_FEATURES
        | MAX_LEAF_NODES
        | MIN_IMPURITY_DECREASE
        | CCP_ALPHA
        | BOOTSTRAP
        | {"oob_score": [True, False]}
        | WARM_START
        # class_weight
        | {"max_samples": {"value": 0.5, "min": 0, "max": 1, "type": PType.CONT}},  # default=None, int (count) or float (percent).  If bootstrap is True, the number of samples to draw from X to train each base estimator.
    "HistGradientBoostingClassifier":
        # "scoring": CLASSIFICATION_METRICS.copy(),  # TODO: fix it! which type of scoring should i use?
        # я думаю это плохая идея, когда скоринг всё время меняется
        MAX_LEAF_NODES
        | MAX_DEPTH
        | MIN_SAMPLES_LEAF
        | {"max_bins": {"value": 255, "min_eq": 2, "max_eq": 255, "add": 128, "type": PType.INT}}
        | {"loss": ["auto"]}  # default='auto', also available binary_crossentropy and categorical_crossentropy
        | {"learning_rate": {"value": 0.1, "type": PType.CONT, "min": .0}}
        | MAX_ITER
        | VALIDATION_FRACTION
        | TOL
        | {"l2_regularization": {"type": PType.CONT, "value": 0.1, "min_eq": 0, "add": 10}}
        | WARM_START
        | N_ITER_NO_CHANGE
        | EARLY_STOPPING,
    "LogisticRegression":
        # TODO: class_weight dict or "balanced", default=None
        # | {"multi_class": ["ovr", "multinomial", "auto"]}
        [
            # ValueError: Solver newton-cg supports only dual=False
            # ValueError: Solver saga supports only dual=False
            # Unsupported set of arguments: The combination of penalty='l1' and loss='logistic_regression' are not supported when dual=True
            # ValueError: Solver saga supports only dual=False, got dual=True
            # ValueError: Solver lbfgs supports only 'l2' or 'none' penalties, got elasticnet penalty.
            {"solver": ["newton-cg", "lbfgs", "sag"]}
            | {"penalty": ["l2", "none"]}
            | C_1 | FIT_INTERCEPT | TOL | MAX_ITER | WARM_START,

            {"solver": ["liblinear"]}
            | {"penalty": ["l1"]}
            | LIBLINEAR_INTERCEPT_SCALING
            | C_1 | FIT_INTERCEPT | TOL | MAX_ITER | WARM_START,

            {"solver": ["saga"]}
            | {"penalty": ["l1", "l1", "none"]}
            | C_1 | FIT_INTERCEPT | TOL | MAX_ITER | WARM_START,

            {"solver": ["saga"]}
            | {"penalty": ["elasticnet"]}
            | ELASTICNET_L1_RATIO
            | C_1 | FIT_INTERCEPT | TOL | MAX_ITER | WARM_START,
        ],
    "LGBMClassifier":
        {"boosting_type": ["gbdt", "dart", "goss", "rf"]}  # default gbdt
        | {"num_leaves": {"value": 31, "type": PType.INT, "min": 1}}  # default: 31
        | MAX_DEPTH
        | {"learning_rate": {"value": 0.1, "type": PType.CONT, "min": 0}}  # default 0.1
        | N_ESTIMATORS
        | {"subsample_for_bin": {"value": 200000, "type": PType.INT}} # default=200000
        # TODO: objective
        # TODO: class_weight
        # TODO: min_split_gain (float, optional (default=0.)) – Minimum loss reduction required to make a further partition on a leaf node of the tree.
        # TODO: min_child_weight (float, optional (default=1e-3)) – Minimum sum of instance weight (hessian) needed in a child (leaf).
        # TODO: min_child_samples (int, optional (default=20)) – Minimum number of data needed in a child (leaf).
        # TOOD: colsample_bytree (float, optional (default=1.)) – Subsample ratio of columns when constructing each tree.
        # TODO: reg_alpha (float, optional (default=0.)) – L1 regularization term on weights.
        # TODO: reg_lambda (float, optional (default=0.)) – L2 regularization term on weights.
        # TODO: importance_type (string, optional (default='split')) – The type of feature importance to be filled into feature_importances_. If "split", result contains numbers of times the feature is used in a model. If "gain", result contains total gains of splits which use the feature.
        | {"subsample": {"value": .5, "type": PType.CONT, "min": 0, "max": 1}}  # Subsample ratio of the training instance. Aliases: sub_row, bagging, bagging_fraction
        | {"subsample_freq": {"value": 1, "add": 10, "type": PType.INT, "min": 0}},  # Frequence of subsample, <=0 means no enable. alias: bagging_freq
    "SVC":
        C_1   # насколько разделяющая поверхность будет гладкой(кривой)
        | {"kernel": ["poly", "rbf", "sigmoid", "linear"]}
        # TODO: add callable to 'kernel'
        | {"degree": {"value": 3, "min_eq": 1, "type": PType.INT}}  # int, optional (default=3)
        | {"gamma": {"value": 1., "min": 0, "type": PType.CONT, "add": 10}}  # при больших gamma, SVM будет опираться только на ближайшие объекты
        # TODO: add float values for 'gamma'
        | {"coef0": {"value": 0.1, "min_eq": 0.0, "add": [.0, 0.5], "type": PType.CONT}}   # float, optional (default=0.0)
        | {"shrinking": [True, False]}  # default=True
        | {"probability": [True, False]}  # default=False
        | TOL
        | MAX_ITER
        # TODO: add cache_size float, optional
        # TODO: add class_weight {dict, "balanced"}, optional
        | {"break_ties": [True, False]},
    "LinearSVC":
        # TODO: class weight
        # | {"multi_class": ["ovr", "crammer_singer"],  # default ovr
        # The combination of penalty='l1' and loss='hinge' is not supported.
        # The combination of penalty='l2' and loss='hinge' are not supported when dual=False
        # The combination of penalty='l1' and loss='squared_hinge' are not supported when dual=True
        [
            {"penalty": ["l2"]}
            | MAX_ITER
            | FIT_INTERCEPT
            | TOL
            | {"dual": [True]}
            | C_1
            | {"loss": ["hinge", "squared_hinge"]}
            | LIBLINEAR_INTERCEPT_SCALING,

            {"penalty": ["l2"]}
            | MAX_ITER
            | FIT_INTERCEPT
            | TOL
            | DUAL
            | C_1
            | {"loss": ["squared_hinge"]}
            | LIBLINEAR_INTERCEPT_SCALING,

            {"penalty": ["l1", "l2"]}
            | MAX_ITER
            | FIT_INTERCEPT
            | TOL
            | {"dual": [False]}
            | C_1
            | {"loss": ["squared_hinge"]}
            | LIBLINEAR_INTERCEPT_SCALING,
        ],
    "PassiveAggressiveClassifier":
        C_1
        | FIT_INTERCEPT
        | MAX_ITER
        | TOL
        | EARLY_STOPPING
        | VALIDATION_FRACTION
        | N_ITER_NO_CHANGE
        | SHUFFLE
        # TODO: add losss tring, optional
        | WARM_START
        # TODO: add class_weightdict, {class_label: weight} or “balanced” or None, optional
        | {"average": [True, False, 5, 10]},  # bool or int
    "QuadraticDiscriminantAnalysis":
        {"reg_param": {"value": 1.0, "type": PType.CONT}}  # Regularizes the per-class covariance estimates by transforming S2 as S2 = (1 - reg_param) * S2 + reg_param * np.eye(n_features), where S2 corresponds to the scaling_ attribute of a given class.
        | TOL
        | {"store_covariance": [True, False]},
    "DecisionTreeClassifier":
        # я не буду использовать ccp_alpha потому, то дерево будет обрезаться
        # само и не имеет смысл указывать ограничение длинны или типа того
        # using default criterion
        {"splitter": ["best", "random"]}
        | MAX_DEPTH
        | MIN_SAMPLES_SPLIT
        | MIN_SAMPLES_LEAF
        | MIN_WEIGHT_FRACTION_LEAF
        | TREE_MAX_FEATURES
        # random_state
        | MAX_LEAF_NODES
        | MIN_IMPURITY_DECREASE,
    "KBinsDiscretizer":
        {"n_bins": {"type": PType.INT, "value": 5, "min_eq": 2, "add": [10]}}
        | {"strategy": ["uniform", "quantile", "kmeans"]},
    "PolynomialFeatures":
        {"degree": [2, 3, (2, 3)]}
        | {"interaction_only": [True, False]}
        | {"include_bias": [True, False]},
    "MinMaxScaler":
        {"feature_range": [(0, 1), (-1, 1)]}
        | {"copy": [True, False]}
        | {"clip": [True, False]},
    # TODO: StandardScaler
    # "PCA": {
    #     "parameters_description":
    #         {"n_components": {"type": PType.CONT, "min_eq": 0, "max_eq": 1, "value": 0.5}}
    #         | {"copy": [True, False]}
    #         | {"whiten": [True, False]}
    #         | {"svd_solver": ["auto", "full", "arpack", "randomized"]}
    #         | TOL  # Tolerance for singular values computed by svd_solver == ‘arpack’. Must be of range [0.0, infinity)
    #         | {"iterated_power": {"type": PType.INT, "min": 0, "value": 5}}  # Number of iterations for the power method computed by svd_solver == ‘randomized’. Must be of range [0, infinity).
    #         # random_state=None)
    # },
    # TODO: CategoricalNB
    # TODO: NearestCentroid
    # TODO: NuSVC
    # TODO: CalibratedClassifierCV ???
}
