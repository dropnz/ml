from functools import cached_property
import random

import numpy as np
from loguru import logger

from ml import PType


class Parameter:
    def __init__(self, description):
        self._description = description

    @cached_property
    def description(self):
        return self._description

    @cached_property
    def is_list(self):
        return isinstance(self.description, list)

    @cached_property
    def default_value(self):
        """ Default value. Described by 'valued' key """
        if not self.is_list:
            return self.description["value"]

    @cached_property
    def additional_values(self):
        """ Values describen by 'add' key """
        if self.is_list:
            return self.description.copy()
        else:
            add_values = self.description.get("add", [])
            return add_values if isinstance(add_values, list) else [add_values]

    @cached_property
    def values(self):
        """ default value + additional values"""
        values = [self.default_value] if self.default_value else []
        values.extend(self.additional_values)
        return values

    @cached_property
    def type(self):
        if self.is_list:
            return PType.DISCRETE
        else:
            return self.description["type"]

    def _apply_filters(self, mutated_values):
        filters = set(("min", "max", "min_eq", "max_eq"))
        if not filters & set(self.description.keys()):
            return set(mutated_values)
        original_values = mutated_values.copy()
        filter_dict_to_print = {}
        for limit in filters:
            if limit not in self.description:
                continue
            limit_value = self.description[limit]
            filter_dict_to_print[limit] = limit_value
            _filtered = set()
            for value in mutated_values:
                if limit == "min":
                    if value > limit_value:
                        _filtered.add(value)
                elif limit == "max":
                    if value < limit_value:
                        _filtered.add(value)
                elif limit == "min_eq":
                    _filtered.add(value if value >= limit_value else limit_value)
                elif limit == "max_eq":
                    _filtered.add(value if value <= limit_value else limit_value)
                else:
                    raise ValueError(f"unknown limit type: {limit}")
            mutated_values = _filtered.copy()
        logger.debug(" - apply filters {} for {} -> {}", filter_dict_to_print, original_values, mutated_values)
        return mutated_values

    def _get_additional_parameters(self, values, k):
        """
        Get k values from "value" or "add" wich differs from `values` parameter.
        @values: set or primitive
        """
        values = values if isinstance(values, set) else set([values])
        additional_values = set(self.values) - values
        additional_values_count = min(1, len(additional_values))
        return set(random.sample(additional_values, k=additional_values_count))

    def _mutate_discrete(self, the_last_value, score_got_worse, is_initial, **kwargs):
        if is_initial:
            mutated_values = self._get_additional_parameters(the_last_value, k=6)
            mutated_values.add(the_last_value)
            return list(mutated_values)
        elif score_got_worse:
            mutated_values = self._get_additional_parameters(the_last_value, k=4)
            mutated_values.add(the_last_value)
            return list(mutated_values)
        else:
            return [the_last_value]

    def _mutate_cont_or_int(self, the_last_value, mutation_factor, is_initial, **kwargs):
        diff = round(the_last_value * mutation_factor, 4)
        if self.type == PType.INT:
            diff = int(diff)
        if diff == 0:
            diff = 1 if self.type == PType.INT else 0.1

        mutated_values = {the_last_value - diff, the_last_value + diff}
        if self.type == PType.CONT:
            mutated_values = {round(v, 4) for v in mutated_values}
        mutated_values = self._apply_filters(mutated_values)
        if len(mutated_values) == 1:
            _value = list(mutated_values)[0]
            mutated_values = self._apply_filters([_value - diff, _value + diff])
            del _value

        mutated_values.add(the_last_value)  # add original value to mutated values.
                                            # ideally we should have 3 values

        # min_values_count = 4 if score_got_worse else 3
        min_values_count = 3

        # if score_got_worse:
        #     for _additional_value in self._get_additional_parameters(mutated_values, k=2):
        #         logger.info(" + add additional value {} (score got worse)", _additional_value)
        #         mutated_values.add(_additional_value)

        if len(mutated_values) < min_values_count:
            _mean = np.mean(list(mutated_values))
            _mean = int(_mean) if self.type == PType.INT else round(_mean, 4)
            # logger.info(" + added mean of values: {}", _mean)
            mutated_values.add(_mean)

        if len(mutated_values) < min_values_count:
            _additional_value = self._get_additional_parameters(mutated_values, k=1)
            if _additional_value:
                _additional_value = list(_additional_value)[0]
                _mean = np.mean((the_last_value, _additional_value))
                _mean = int(_mean) if self.type == PType.INT else round(_mean, 5)
                # logger.info(" + added mean of value and additional value: {}", _mean)
                mutated_values.add(_mean)
        return list(mutated_values)


    def mutate(self, the_last_value, *args, **kwargs):
        if the_last_value is None:
            if self.default_value is not None:
                the_last_value = self.default_value
            else:
                the_last_value = random.sample(self.additional_values, k=1)[0]

        if self.type == PType.DISCRETE:
            return self._mutate_discrete(the_last_value=the_last_value, *args, **kwargs)
        else:
            return self._mutate_cont_or_int(the_last_value=the_last_value, *args, **kwargs)
