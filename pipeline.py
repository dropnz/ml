# TODO: brute random seed for tree-related models
# TODO: имплементировать трансформеры и скалеры для всех типов моделей (see __init__)
# TODO: сохранять удачные модели, если они превзошли
# TODO: когда стоит добавлять нелинейность в параметры (polynomial??)
# TODO: пока скор увеличивается, НЕ НУЖНО МУТИРОВАТЬ КАТЕГОРИАЛЬНЫЕ ПАРАМЕТРЫ

from pprint import pprint
import re
from functools import cached_property
import random
from dataclasses import dataclass
import itertools
from collections import defaultdict

from loguru import logger
from oslo_utils import reflection
from sklearn.base import BaseEstimator
from sklearn.model_selection import RandomizedSearchCV

from ml import estimators_description
from ml import PType
from ml.parameter import Parameter

class NoDescriptionException(Exception): pass
class NoPredictProbaException(Exception): pass
class NanValidationResult(Exception):
    """ cross_val_score returned NaN """


def _get_estimator_class_name(estimator):
    return reflection.get_class_name(estimator).split(".")[-1]


class Snapshot:
    def __init__(self, model):
        self._model = model   # fitted RandomizedSearchCV
        # self._mutation_factor = mutation_factor

    def __str__(self):
        return f'Snapshot of {self.name} score:{self.score}'

    @property
    def estimator(self):
        return self._model.best_estimator_

    @property
    def params(self):
        return self._model.best_params_

    @property
    def score(self):
        return round(self._model.best_score_, 3)

    @property
    def name(self):
        return _get_estimator_class_name(self.estimator.steps[-1][-1])


class Parameters:
    def __init__(self, pipeline, verbose=False):
        self._pipeline = pipeline
        self._verbse = verbose

    def _get_description_for_step(self, step_name, estimator):
        ''' Helps init description for pipeline '''
        description = []
        estimator_class_name = _get_estimator_class_name(estimator)
        if estimator_class_name not in estimators_description:
            raise NoDescriptionException(estimator_class_name)
        parameters_combinations = estimators_description[estimator_class_name].copy()
        if isinstance(parameters_combinations, dict):
            parameters_combinations = [parameters_combinations]
        for parameters_description in parameters_combinations:
            description.append({})
            for parameter_name in parameters_description:
                key = f"{step_name}__{parameter_name}"
                description[-1][key] = parameters_description[parameter_name]
                # logger.info(f"{key} -> {description[-1][key]}")
        return description

    @cached_property
    def description(self):
        description = []
        for step_name, estimator in self._pipeline.steps:
            try:
                description.append(
                    self._get_description_for_step(step_name, estimator)
                )
                if self._verbse:
                    logger.info(f'Found description for {_get_estimator_class_name(estimator)}')
            except NoDescriptionException as exc:
                if self._verbse:
                    logger.warning(f'No description for {exc}')
        return description

    @cached_property
    def grouped_description(self):
        grouped_description = defaultdict(list)
        for params_combination in self.description:
            for description_dict in params_combination:
                if not description_dict:
                    continue
                grouped_description[tuple(description_dict.keys())[0].split('__')[0]].append({})
                for full_name, description in description_dict.items():
                    step_name, name = full_name.split('__')
                    grouped_description[step_name][-1][name] = description
        return grouped_description

    def _get_initial_parameters(self, mutation_factor, score_got_worse, the_best_params=None):
        initial_parameters = []
        for params_combination in itertools.product(*self.description):
            initial_parameters.append({})
            for description_dict in params_combination:
                for full_name, description in description_dict.items():
                    name = full_name.split('__')[-1]
                    if name == "random_state":
                        continue
                    parameter = Parameter(description)
                    initial_parameters[-1][full_name] = parameter.mutate(
                        the_last_value=None,
                        mutation_factor=mutation_factor,
                        score_got_worse=False,
                        is_initial=True,
                    )
        logger.info(f'Initial parameters: {len(initial_parameters)} combinations')
        return initial_parameters

    def _is_description_match_params(self, params, description):
        if params.keys() != description.keys():
            return False

        for name, param_description in description.items():
            if isinstance(param_description, list) and len(param_description) == 1:
                if params[name] != param_description[0]:
                    return False

        return True

    def _get_description(self, step_name, params):
        suitable_description = None
        other_descriptions = []

        for description in self.grouped_description[step_name]:
            if self._is_description_match_params(params, description):
                suitable_description = description
                break
            else:
                other_descriptions.append(description)
                # import ipdb; ipdb.set_trace()

        if not suitable_description:
            raise RuntimeError(f'No suitable description for {step_name}')
        return suitable_description, other_descriptions

    def _mutate_parameters(self, mutation_factor, score_got_worse, the_best_params):
        new_parameters = [{}]

        # some parameters may have several descriptions (if there is mutually exclusive combinations)
        # for example: LinearSVC. Some combinations of `penalty` and `loss` aren't compatible 
        # We will use another description if score got worse

        # convert {'estimator__tol': 0.1} -> {'estimator': {'tol': 0.1}}
        # TODO: move to another method
        grouped_parameters = defaultdict(dict)
        for full_name, value in the_best_params.items():
            step_name, name = full_name.split('__')
            grouped_parameters[step_name][name] = value

        for step_name, old_params in grouped_parameters.items():
            suitable_description, other_descriptions = self._get_description(step_name, old_params)
            # TODO: check other_descriptions if score get worse
            for param_name in old_params:
                param = Parameter(suitable_description[param_name])
                mutated_param = param.mutate(
                    the_last_value=old_params[param_name],
                    mutation_factor=mutation_factor,
                    score_got_worse=score_got_worse,
                    is_initial=False,
                )
                logger.info(f'{step_name}__{param_name} {old_params[param_name]} -> {mutated_param}')
                new_parameters[-1][f'{step_name}__{param_name}'] = mutated_param
        return new_parameters

    def mutate(self, warmup, *args, **kwargs):
        if warmup:
            return self._get_initial_parameters(*args, **kwargs)
        else:
            return self._mutate_parameters(*args, **kwargs)


class RandomizedSearchMutation:
    def __init__(self, pipeline, X, y, **kwargs):
        self._pipeline = pipeline
        self._n_iter = kwargs.get("n_iter")
        self._scoring = kwargs.get("scoring", "f1")
        self._max_generation = kwargs.get("max_generation", 3)
        self._cv = kwargs.get("cv", 10)
        self._mutation_factor = 0.95
        self._mutation_delta = -0.15
        self._n_jobs = kwargs.get("n_jobs")  # None by default. means what????
        self._X = X
        self._y = y
        self._snapshots = []

    @property
    def n_iter(self):
        ''' Number of iterations for RandomizedSearchCV '''
        if self._n_iter is None:
            # let's use count of parameter descriptions as default value for _n_iter + some const
            self._n_iter = 3
            for params_combination in Parameters(self._pipeline).description:
                self._n_iter += len(params_combination)
            logger.info(f'Set n_iter as {self._n_iter}')
        return self._n_iter

    @n_iter.setter
    def n_iter(self, new_value):
        new_value = 5 if new_value < 5 else new_value
        logger.info(f'n_iter {self._n_iter} -> {new_value}')
        self._n_iter = new_value

    def _restore_snapshots(self, snapshot):
        pass

    def search_the_best(self):
        score_got_worse = False
        parameters = Parameters(self._pipeline, verbose=True)
        for generation in range(self._max_generation):
            logger.error(f'Iteration {generation} of {self._max_generation-1}')
            self._mutation_factor += self._mutation_delta
            if self._mutation_factor < 0.01:
                raise RuntimeError("mutation percent is too low")
            logger.info(f'Run randomized search n_iter={self.n_iter} cv={self._cv}')
            distribution = parameters.mutate(
                mutation_factor=self._mutation_factor,
                warmup=generation==0,
                score_got_worse=score_got_worse,
                the_best_params=None if not self._snapshots else self._snapshots[-1].params,
            )
            clf = RandomizedSearchCV(
                estimator=self._pipeline,
                param_distributions=distribution,
                n_iter=self.n_iter*3 if generation == 0 else self.n_iter,
                scoring=self._scoring,
                n_jobs=self._n_jobs,
                cv=self._cv,
                random_state=0,
                return_train_score=True,
                error_score="raise",
                refit=True,
            ).fit(self._X, self._y)

            snapshot = Snapshot(clf)
            logger.info(snapshot)
            if self._snapshots and snapshot.score <= self._snapshots[-1].score:
                score_got_worse = True
                logger.warning("score got worse")
                self.n_iter += 5
                parameters = Parameters(self._snapshots[-1].estimator)
            else:
                self.n_iter -= 3
                score_got_worse = False
                self._snapshots.append(snapshot)
        return self._snapshots[-1]
