"""
TODO: links to read info
TODO: notes
"""

import pandas as pd
from sklearn.base import BaseEstimator
from sklearn.base import TransformerMixin
from ml import estimators_description
import category_encoders


class EncodeCategorical(BaseEstimator, TransformerMixin):
    _enc_map = {
        # "freq": has been released here
        "ohe": category_encoders.OneHotEncoder,
        # bayesian-based encoders
        "te": category_encoders.TargetEncoder,
        "looe": category_encoders.LeaveOneOutEncoder,  # 'sigma' adds noise into normaal distribution: mean=0, deviation=sigma
        "woe": category_encoders.WOEEncoder,
        "jse": category_encoders.JamesSteinEncoder,
        "mee": category_encoders.MEstimateEncoder,
        "cbe": category_encoders.CatBoostEncoder,
        "glmme": category_encoders.GLMMEncoder,    # применяет линейную регрессию к target???
        # contrast encoders
        "se": category_encoders.SumEncoder,
        "pe": category_encoders.PolynomialEncoder,
        "bde": category_encoders.BackwardDifferenceEncoder,
        "he": category_encoders.HelmertEncoder,
    }

    _enc_add_many_features = ["se", "ohe", "pe", "he", "bde"]

    @classmethod
    def encoders(cls):
        return list(cls._enc_map.keys()) + ["freq"]

    def __init__(self, method=None, columns=None, save_original_features=False):
        self.method = method
        self.columns = columns
        self.save_original_features = save_original_features
        self.nominal_features_transformed_ = None

    def fit(self, X, y=None):
        if self.method == "freq":
            return self
        params = {
            "cols": self.columns,
            "handle_missing": "value",
        }
        if self.method in self._enc_add_many_features:
            params |= {"drop_invariant": True}
        if self.method == "ohe":
            params |= {"use_cat_names": True}
        encoder = self._enc_map[self.method](**params)
        setattr(self, f"_{self.method}", encoder)
        encoder.fit(X, y)
        return self

    def transform(self, X, *args, **kwargs):
        if "freq" == self.method:
            Xtr = X.copy()
            for column in self.columns:
                Xtr[column] = X[column].map(X.groupby(column).size() / len(X))
                Xtr[column].fillna(0, inplace=True)
        else:
            encoder = getattr(self, f"_{self.method}")
            Xtr = encoder.transform(X)
        if self.method in self._enc_add_many_features:
            _created_features = []
            _deleted_features = [m["col"] for m in encoder.mapping]
            [_created_features.extend(list(m["mapping"].columns)) for m in encoder.mapping]
            new_features = Xtr[_created_features]
        else:
            new_features = Xtr[self.columns].rename(columns=lambda x: x+f"_{self.method}")
            Xtr = Xtr.drop(self.columns, axis=1)
        Xret = X.copy()
        if not self.save_original_features:
            Xret = Xret.drop(self.columns, axis=1)
        Xret = pd.concat((new_features, Xret), axis=1)
        self.nominal_features_transformed_ = list(new_features.columns)
        return Xret


estimators_description['EncodeCategorical'] = {
    "method": ["ohe", "te", "looe", "woe", "jse", "mee", "cbe", "glmme", "se", "pe", "bde", "he", "freq"],
}
