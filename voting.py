from functools import cached_property
import itertools
import re
import random

from oslo_utils import reflection
from sklearn import ensemble
from sklearn import model_selection
from loguru import logger
import numpy as np

from . import estimators_description
from . import SearchHistory


class BruteVoting:
    _estimator_score_cache = {}

    def __init__(self, /, X, y, pipelines, **kwargs):
        self._cv = kwargs.get("cv", 5)
        self._voting = kwargs["voting"]
        self._scoring = kwargs["scoring"]
        self._n_jobs = kwargs.get("n_jobs")
        self._X = X
        self._y = y
        self._pipelines = pipelines
        self._history = None
        self._min_estimators = kwargs.get("min_estimators", min(2, len(self.suitable_pipelines)))
        self._should_filter_by_type = kwargs.get("should_filter_by_type", False)
        self._max_estimators = kwargs.get("max_estimators", min(3, len(self.suitable_pipelines)))
        self._checked_estimators_sets = None

    def _filter_estimators_by_type(self, estimators):
        type_to_estimators = {}  # structure {algorithm_type0: [{estimator: None, score: None}, ..], ..}
        for estimator in estimators:
            class_name = reflection.get_class_name(estimator).split(".")[-1]
            alg_type = estimators_description[class_name].get("algorithm_type")
            if alg_type is None:
                raise RuntimeError(f"{class_name} doesn't have a 'algorithm_type'")
            elif alg_type not in type_to_estimators:
                type_to_estimators[alg_type] = []
                # TODO: не делать всё время проверку скора. можно просто запомнить и всё

            if class_name in self._estimator_score_cache:
                score = self._estimator_score_cache[class_name]
            else:
                score = np.mean(model_selection.cross_val_score(estimator, self._X, self._y, cv=self._cv, scoring=self._scoring))
                self._estimator_score_cache[class_name] = score

            type_to_estimators[alg_type].append({"estimator": estimator, "score": score})

        # type_to_estimators will have such structure: {algorithm_type0: best_estimator, ...}
        for alg_type in type_to_estimators:
            type_to_estimators[alg_type].sort(key=lambda x: x["score"], reverse=True)
            best_of_type = type_to_estimators[alg_type][0]
            logger.debug("best estimator of type {} is {} (score: {})",
                         alg_type, reflection.get_class_name(best_of_type["estimator"]),
                         best_of_type["score"])
            type_to_estimators[alg_type] = best_of_type["estimator"]
        filtered_estimators = list(type_to_estimators.values())
        logger.debug("estimators were filtered by type {} -> {}", len(self._pipelines), len(filtered_estimators))
        return filtered_estimators

    @cached_property
    def suitable_pipelines(self):
        if self._voting == "hard":
            return self._pipelines
        elif self._voting == "soft":
            suitable_estimators = []
            for est in self._pipelines:
                if not hasattr(est, "predict_proba"):
                    logger.error("{} doesn't have `predict_proba` method. ignore it", est)
                else:
                    suitable_estimators.append(est)
            return suitable_estimators
        else:
            raise RuntimeError(f"unknown voting type: {self._voting}")

    def get_top_ensembles(self, k):
        if self._checked_estimators_sets is None:
            self._checked_estimators_sets = {}
            self._search_ensemble()
            _checked_estimators_sets = self._checked_estimators_sets
            self._checked_estimators_sets = sorted([ensemble_set for ensemble_set in _checked_estimators_sets],
                                                   reverse=True,
                                                   key=lambda x: _checked_estimators_sets[x]["score"])
            self._checked_estimators_sets = [_checked_estimators_sets[_set]["estimators"] for _set in self._checked_estimators_sets]
            del _checked_estimators_sets
        logger.info("found {} ensembles", len(self._checked_estimators_sets))
        for model_combination in self._checked_estimators_sets[:k]:
            weights = self._get_best_weights_for_ensemble(model_combination)
            estimators_str = "_".join([reflection.get_class_name(pipe.steps[-1][-1]).split(".")[-1] for pipe in model_combination])
            model = ensemble.VotingClassifier(
                estimators=[(reflection.get_class_name(pipe.steps[-1][-1]).split(".")[-1], pipe) for pipe in model_combination],
                voting=self._voting,
                weights=weights)
            yield SearchHistory(
                model=model,
                name=f"{estimators_str}_" + "_".join(map(str, weights)).replace(".", ""),
                score=np.mean(model_selection.cross_val_score(model, self._X, self._y, cv=self._cv, scoring=self._scoring)),
                parameters=None)

    @staticmethod
    def _get_weights_combinations(estimators_count, initial_w_value=0.1, increase_w_per_interation=0.1):
        def generate_combination(n, maximum):
            w = np.array([initial_w_value for _ in range(n)])
            while np.sum(w) < maximum:
                ix = random.randint(0, n-1)
                w[ix] += increase_w_per_interation
                w[ix] = round(w[ix], 3)
            return tuple(w)

        iterations_left = 100
        found_combinations = set()
        if estimators_count == 1:
            return [(1,)]
        maximums_to_count = {0.9: 1, 1.0: 3, 1.1: 1}
        max_combinations_count = len(maximums_to_count) * estimators_count * 2
        while True:
            for maximum in maximums_to_count:
                for _ in range(maximums_to_count[maximum]):
                    if iterations_left <= 0 or len(found_combinations) >= max_combinations_count:
                        break
                    iterations_left -= 1
                    w_combination = generate_combination(estimators_count, maximum=maximum)
                    found_combinations.add(w_combination)
                    logger.debug("found combination: {} count:{} max:{}, iter_left:{})",
                                 w_combination, len(found_combinations), max_combinations_count, iterations_left)
            if iterations_left <= 0 or len(found_combinations) >= max_combinations_count:
                break
        return found_combinations

    def _get_best_weights_for_ensemble(self, estimators):
        best_score = None
        best_weights = None
        estimators_str = "_".join(sorted(reflection.get_class_name(pipe.steps[-1][-1]).split(".")[-1] for pipe in estimators))
        _all_generated_weights = self._get_weights_combinations(len(estimators))
        for w_ix, weights in enumerate(_all_generated_weights):
            model = ensemble.VotingClassifier(
                estimators=[(reflection.get_class_name(pipe.steps[-1][-1]).split(".")[-1], pipe) for pipe in estimators],
                voting=self._voting,
                weights=weights,
            )
            score = np.mean(model_selection.cross_val_score(model, self._X, self._y, cv=self._cv, scoring=self._scoring))
            if best_score is None or score > best_score:
                best_score = score
                best_weights = weights
                logger.info("  + {} better w:{} score:{} ({} of {})",
                            estimators_str, str(best_weights), round(best_score, 4),
                            w_ix, len(_all_generated_weights))
            else:
                logger.debug("  - {} skip w:{} score:{}", estimators_str, weights, round(score, 4))
        return best_weights

    def _search_ensemble(self):
        for count_estimators in range(self._min_estimators, self._max_estimators+1):
            for model_combination in itertools.combinations(self.suitable_pipelines, count_estimators):
                if self._should_filter_by_type:
                    model_combination = self._filter_estimators_by_type(model_combination)
                    if len(model_combination) < self._min_estimators:
                        logger.warning("too little of estimators after filter: {}", len(model_combination))
                        continue
                estimators_classes = [reflection.get_class_name(pipe.steps[-1][-1]).split(".")[-1] for pipe in model_combination]
                estimators_set = frozenset(estimators_classes)

                if estimators_set in self._checked_estimators_sets:
                    logger.warning("already checked: {}", estimators_set)
                    continue
                model = ensemble.VotingClassifier(
                    estimators=[(reflection.get_class_name(pipe.steps[-1][-1]).split(".")[-1], pipe) for pipe in model_combination],
                    voting=self._voting)
                score = np.mean(model_selection.cross_val_score(model, self._X, self._y, cv=self._cv, scoring=self._scoring))
                estimators_str = "_".join([reflection.get_class_name(pipe.steps[-1][-1]).split(".")[-1] for pipe in model_combination])
                logger.info("{} {} score: {}", self._voting, estimators_str, score)
                self._checked_estimators_sets[estimators_set] = {
                    "score": score,
                    "estimators": model_combination}
